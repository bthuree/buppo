#!/usr/bin/make -f

CONFDIR = etc/buppo
DEFDIR = etc/default
BINDIR = usr/sbin
LOGDIR = var/log/buppo
SPOOLDIR = var/spool/buppo
STATEDIR = var/lib/buppo
MANDIR = usr/share/man

# To generate the man pages
DB2MAN=/usr/share/xml/docbook/stylesheet/nwalsh/manpages/docbook.xsl
XP=xsltproc -''-nonet


man: buppo.8 buppo.conf.5
buppo.8: docs/buppo.manpage.8.xml
	$(XP) -o debian/buppo.8 $(DB2MAN) docs/buppo.manpage.8.xml
buppo.conf.5: docs/buppo.conf.manpage.5.xml
	$(XP) -o debian/buppo.conf.5 $(DB2MAN) docs/buppo.conf.manpage.5.xml



install:
	### Fix the version number
	cp bin/buppo bin/buppo.preMake
	_buppo_directory="`pwd`"; \
	_buppo_build_time="`date`";  \
	_buppo_version_number="`basename $${_buppo_directory} | awk -F "-" '{print $$2}'`"; \
	sed "s/BUPPO_VERSION_NUMBER=.*/BUPPO_VERSION_NUMBER=\"$${_buppo_version_number}\"/1" bin/buppo > bin/buppo.$$$$; \
	mv bin/buppo.$$$$  bin/buppo; \
	sed "s/# Id (.*/# Id ($${_buppo_version_number} - $${_buppo_build_time}) #/1" bin/buppo > bin/buppo.$$$$; \
	mv bin/buppo.$$$$  bin/buppo
	chmod a+x bin/buppo

	# Create the directories
	install -d $(DESTDIR)/$(CONFDIR)
	install -d $(DESTDIR)/$(DEFDIR)
	install -d $(DESTDIR)/$(SPOOLDIR)
	install -d $(DESTDIR)/$(STATEDIR)
	install -d $(DESTDIR)/$(LOGDIR)
	install -d $(DESTDIR)/$(BINDIR)
	install -d $(DESTDIR)/$(MANDIR)/man8
	install -d $(DESTDIR)/$(MANDIR)/man5

	install -m 700 -d $(DESTDIR)/$(CONFDIR)/pre_scripts
	install -m 700 -d $(DESTDIR)/$(CONFDIR)/post_scripts
	install -m 700 -d $(DESTDIR)/$(CONFDIR)/readme_txt

	# Install the scripts
	install -m 755 bin/buppo $(DESTDIR)/$(BINDIR)/

	# Install the config files
	install -m 644 etc/buppo.conf $(DESTDIR)/$(CONFDIR)
	install -m 644 debian/buppo-default $(DESTDIR)/$(DEFDIR)/buppo
	
	# Install the readme files
	install -m 644 etc/readme_txt/full_readme.txt $(DESTDIR)/$(CONFDIR)/readme_txt/
	install -m 644 etc/readme_txt/incremental_readme.txt $(DESTDIR)/$(CONFDIR)/readme_txt/
	install -m 644 etc/readme_txt/differential_readme.txt $(DESTDIR)/$(CONFDIR)/readme_txt/

	# Install the sample pre_script file
	install -m 755 etc/pre_scripts/10_sample $(DESTDIR)/$(CONFDIR)/pre_scripts/
	install -m 644 etc/pre_scripts/ReadMe.txt $(DESTDIR)/$(CONFDIR)/pre_scripts/
	install -m 755 etc/post_scripts/10_sample $(DESTDIR)/$(CONFDIR)/post_scripts/
	install -m 644 etc/post_scripts/ReadMe.txt $(DESTDIR)/$(CONFDIR)/post_scripts/

clean:
	# Remove the scripts
	-rm -f  $(DESTDIR)/$(BINDIR)/buppo
	# Remove the configfiles
	-rm -f $(DESTDIR)/$(CONFDIR)/buppo.conf
	-rm -f $(DESTDIR)/$(DEFFDIR)/buppo
	# Remove the Readme files
	-rm -rf $(DESTDIR)/$(CONFDIR)/readme_txt/
	# Remove PreScript files
	-rm -rf $(DESTDIR)/$(CONFDIR)/pre_scripts/
	-rm -rf $(DESTDIR)/$(CONFDIR)/post_scripts/
	# Remove the statedir and it's contents
	-rm -rf $(DESTDIR)/$(STATEDIR)
	# Remove the logdir and it's contents
	-rm -rf $(DESTDIR)/$(LOGDIR)
	# Remove the spooldir  and it's contents
	-rm -rf $(DESTDIR)/$(SPOOLDIR)

	# Finally remove the config directory
	-rmdir $(DESTDIR)/$(CONFDIR)

distclean:
	#-find . -name "*~" | xargs -r --no-run-if-empty rm -vf

check:
	#cd test; python test.py

system-test:
	# cd test; rm -fv state/*; \
	#	../src/logcheck -c ../etc/logcheck.conf \
	#			-l ../etc/logcheck.logfiles \
	#			-r ../rulefiles/linux \
	#			-S state/ -o
