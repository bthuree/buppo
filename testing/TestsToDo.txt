Future test cases that shall be done


Start options
=============
full + incr + auto
version
versionumber
help
quiet
keep
debug
trace
config

Configuration file
===================
backup_name contains .
keep_name contains .
spool_dir contains other directories/files than buppos
directory


Running
=======
full backup
incremental backup relativ to full
	Verify new file and deleted file
incremental backup relativ to last incremental or full
	Verify new file and deleted file
backup not unique
can not get lock
Auto
	first dump
		have to fullfill the full dump window
	Not enough time between dumps
	Time for full, but not according to full dump window
	time for incremental, but not enough time between dumps
	time between incr dumps
	time between full dumps
skip dirs
clean up /tmp and working

Linking
=======
Making sure that the dumps are linked to the proper directory
keep
not_fetched
incremental
day, week, month, quarter, year

Deleting
========
Only delete when configuration file allows us to
Only keeping the number of full dumps according to conf file
Only keeping incr dumps according to full daily dumsp
Only keeping not_fetched dumps according to conf file

Debian
======
Only doing debian packages according to conf file

e-mail
======
verify always
verify error
verify never
