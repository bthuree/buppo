#
# Regular cron jobs for the buppo package
#
# m h dom mon dow 		user    command
#
# Do a default automatic backup every 10 minute. Let buppo handle when it should do Full resp. Incr backup
3-59/10 	* *   *   *		root	/usr/sbin/buppo --quiet auto 

# To run a second instance with a new configuration file
#2,12,22,32,42,52 	* *   *   *		root	/usr/sbin/buppo -c /etc/buppo/buppo.home.conf --quiet auto
