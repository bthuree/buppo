Document: buppo
Title: Debian buppo Manual
Author: <insert document author here>
Abstract: This manual describes what buppo is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/buppo/buppo.sgml.gz

Format: postscript
Files: /usr/share/doc/buppo/buppo.ps.gz

Format: text
Files: /usr/share/doc/buppo/buppo.text.gz

Format: HTML
Index: /usr/share/doc/buppo/html/index.html
Files: /usr/share/doc/buppo/html/*.html

  
