PRE-SCRIPTS
============

The scripts can not contain any "." but "_" is ok.

Input parameter will be 
	1: quiet job? yes --> suppress output
	
The pre-scripts will always be called, no matter if the backup was successfull or not.

The pre-script will have to handle its own error status.

Echo all error messages to stderr.