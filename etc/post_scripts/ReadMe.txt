POST-SCRIPTS
============

The scripts can not contain any "." but "_" is ok.

Input parameter will be 
	1: quiet job? yes --> suppress output
	2: backup name, or empty. If empty --> No backup took place
	
The post-scripts will always be called, no matter if the backup was successfull or not.

The post-script will have to handle its own error status.

Echo error messages to stderr

If there is an error, buppo will ignore it (but write the stderr to errorlog) 
and continue to call the next postscript.
