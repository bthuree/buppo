{{BASENAME}} ReadMe {{CURRTIME}}
========================================================================

Backup done 			: {{TIMESTAMP}}
Backup directories 		: {{BACKUPDIRS}}
Skipped directories		: {{SKIPDIRS}}
Type of backup			: Incremental
Last full dump			: {{LASTFULL}}
Number of files backuped	: {{BACKUPEDFILES}}
Number of deleted files		: {{DELETEDFILES}}
Save list of inst. deb packages	: {{LISTDEBIANPACKAGES}}
No. of inst. debian packages	: {{DEBIANPACKAGES}}

This backup is an incremental dump, that is based upon the last {{INCRRELATIVE}} dump.

To restore a full dump, first restore the last full dump ({{LASTFULL}}), and then restore
the incremental dump that has the timestamp you are interested in.
Each incremental dump is based relative to the last full dump ({{LASTFULL}}) so you only
need to restore one incremental dump.
After restoring, make sure you also delete all the files that were deleted, see Files_To_Be_Deleted.txt

The backup contains the following files...

The common first part of each file in the backup.
BaseName 	= {{COMMONFILENAME}}


Name			What
------------------------------------------------------------------------
afio.bz2		The backup is done relative to /, 
			and tared with afio and compressed with bz2
			This file will only exist if one or more files 
			were backuped.

FileList.txt 		The files that were modified or created after the last full
			backup was done.
			This file will only exist if one or more files 
			were backuped.

Files_To_Be_Deleted.txt	The files that were deleted after the last full backup was done

DPKG.txt.gz		All installed Debian packages are listed in a 
			text file, and then compressed with gzip
			This file will only exist if installed Debian 
			packages were backuped.

readme.txt		This ReadMe file

md5sum.txt		md5sum checksum of each file
