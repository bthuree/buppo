{{BASENAME}} ReadMe {{CURRTIME}}
========================================================================

Backup done 			: {{TIMESTAMP}}
Backup directories 		: {{BACKUPDIRS}}
Skipped directories		: {{SKIPDIRS}}
Type of backup			: Full
Number of files backuped	: {{BACKUPEDFILES}}
Save list of inst. deb packages	: {{LISTDEBIANPACKAGES}}
No. of inst. debian packages	: {{DEBIANPACKAGES}}


The backup contains the following files...

The common first part of each file in the backup.
BaseName 	= {{COMMONFILENAME}}


Name			What
------------------------------------------------------------------------
afio.bz2		The backup is done relative to /, 
			and tared with afio and compressed with bz2
			This file will only exist if one or more files 
			were backuped.

FileList.txt 		The files that are backuped are listed in a file
			This file will only exist if one or more files 
			were backuped.


DPKG.txt.gz		All installed Debian packages are listed in a 
			text file, and then compressed with gzip
			This file will only exist if installed Debian 
			packages were backuped.


readme.txt		This ReadMe file

md5sum.txt		md5sum checksum of each file in this backup session.
