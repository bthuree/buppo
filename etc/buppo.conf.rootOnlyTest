# Configuration file for buppo
#
#	Version 2004-06-09, Bengt Thuree

# The first part of the backup name, defaults to hostname
# "." are not allowed in the BACKUP_NAME.
#	BACKUP_NAME=`hostname`
#	BACKUP_NAME="work"
#	BACKUP_NAME="home"
BACKUP_NAME=`hostname`

# We can specify a subset (backup set) as well.
# "." are not allowed in the BACKUP_SET.
# Default is nothing, just use the BACKUP_NAME
# The name of the backup will be  : BACKUP_NAME or BACKUP_NAME-BACKUP_SET (if set is specified)
# Remember that you can have set specific pre/post-scripts. Just create a directory with the ${BACKUP_SET} name
# in the subdirectory pre_scripts and/or post-scripts and these are located in the same directory as your configuration file.
# You can also specify if you want to run the common and/or set specific pre and/or post-scripts. You do that further down in this file.
#	BACKUP_SET="etc"
#	BACKUP_SET="home"
BACKUP_SET="root"

# Pre/Post scripts
# Specify here if you want to run the pre/post scripts.
# The common option is for running the scripts in the /etc/pre_scripts or /etc/post_scripts directory
# The set option is for running the scripts in the ${BACKUP_SET} subdirectory under the pre/post script directory.
# The order of running the scripts are pre, pre/set, post/set, and post
#	"yes" : Run the scripts in this directory.
#	"no" : do not run any scripts in this directory.
RUN_COMMON_PRE_SCRIPTS="yes"
RUN_SET_PRE_SCRIPTS="yes"
RUN_COMMON_POST_SCRIPTS="yes"
RUN_SET_POST_SCRIPTS="yes"

# Generate a list of installed debian packages
DPKG_LIST="yes"

# List of directories to backup 
#	Relative to root (/)
#	Space separation between directories
# 	Example: BACKUP_DIRS="etc home root var/www var/spool"
BACKUP_DIRS="root  "

# List of directories to NOT backup 
#	Relative to root (/)
#	"|" separation between directories
# 	Example: SKIP_DIRS="var/spool/buppo|var/lib/dpkg|home/overnet"
SKIP_DIRS="var/spool/buppo|var/lib/dpkg|chroot"

# Only locally mounted file system
#	Please bear in mind that if you explicitly specifes a nfs mounted directory in
#	BACKUP_DIRS then it will be backuped as well.
# 	A value of "-mount" will only backup current file system
#	A vaule of "-mount ! -fstype nfs" will only backup current file system, and skip nfs.
#	An empty value "" will backup all files (local + NFS mounted)
#EXCLUDE_FILE_SYSTEMS=""
EXCLUDE_FILE_SYSTEMS="-mount ! -fstype nfs"

# Type of Incremental backups relative to last FULL backup, or relative to last INCREMENTAL backup
# 	differential	--> All files that are change since last backup (either full or incremental).
#	incremental	--> All files that are changed since last FULL backup.
# TYPE_OF_INCR="differential"
TYPE_OF_INCR="differential"

# Between which hours can a full automaticall backup be made (24 hour clock --> 03 = 3AM)
#	BACKUP_WINDOW_LOWER=2 --> The time has to be 2AM or later
#	BACKUP_WINDOW_HIGHER=5 --> The time has to be 5:59AM or earlier
FULL_BACKUP_WINDOW_LOWER="02"
FULL_BACKUP_WINDOW_HIGHER="05"

# Set to number of minutes between automatic full dumps
#	0 	--> No automatical incremental dumps are done
#	X 	--> X minutes between full backups
#	X >= 10 minutes, since the script is run every 10 minutes by default.
#	1440	--> one day (60 * 24)
#	10080	--> one week (60 * 24 * 7)
#	43200	--> 1 month (60 * 24 * 30)
#TIME_BETWEEN_FULL_DUMPS=1440
TIME_BETWEEN_FULL_DUMPS=10080

# Set to number of minutes between automatic incremental dumps
#	0 	--> No automatical incremental dumps are done
#	X 	--> X minutes between incremental backups
#	X >= 10 minutes, since the script is run every10 minutes by default.
#	120	--> 2 hours (60 * 2)
#	360	--> 6 hours (60 * 6)
#	720	--> 12 hours (60 * 12)
#	1440	--> 24 hours (60 * 24)
#TIME_BETWEEN_INCR_DUMPS=120
TIME_BETWEEN_INCR_DUMPS=720

# Specify how many backup links you want in each period directory.
# Specify 0 for not saving any backup links at this intervall.
# When the last backup link has been removed the actual backup file will also be removed.
# The backup will be linked to a period directory as well as to the not_fetched directory.
# If the full dump intervall is 1 week for instance, it will be one week between each new full dump, 
#	so specifying 6 dumps in the DUMP_DAY means 6 weeks of dumps.
# Also, specifying 0 in DUMPS_DAY means no incremental backup links will be in the ncremental directory. 
#	They will, though, be in the not_fetched directory for a while.
# Specifying 1 will mean that eventually the oldest will be erased and replaced with the newest. 
#	The one year old dump will be replaced with todays dump for instance if you specify DUMPS_YEAR=0
DUMPS_DAY=1
DUMPS_WEEK=5
DUMPS_MONTH=4
DUMPS_QUARTER=3
DUMPS_YEAR=2

# Max minutes to keep an unfetched dump
# This is irrelevant of which backup set you are doing. 
# It will simply check in the not_fetched directory for ALL files older than KEEP_UNFETCHED_DUMP_MINUTES min and delete them.
#	360	--> 6 hours (60 * 6)
#	720	--> 12 hours (60 * 6)
#	1440	--> one day (60 * 24)
#	2880	--> two days (60 * 24 * 2)
#	10080	--> one week (60 * 24 * 7)
KEEP_UNFETCHED_DUMP_MINUTES=2880


# Mail account to send log file to
SYSADM=root

# Backup status mail - Report Errors or everything?
#	all	--> Always send an e-mail
#	warning	--> Only send an e-mail on warning or error
#	error	--> Only send an e-mail on error
#	never	--> Never send an e-mail
MAIL_LEVEL="error"

# Log level
#	error		--> Only error messages will be printed out
#	normal		--> Some status messages + "error"
#	trace		--> Trace messages + "normal"
#	debug		--> Debug messages + "trace"
LOG_LEVEL="normal"


# AUTODELETE controls the way buppo will delete old backups
#	YES --> Automatically delete old backups according to the configuration file
#	NO  --> Buppo will not delete any old backups, you will have to manually delete them.
#AUTODELETE=no
AUTODELETE=yes

# Run with NICE level
# Not implemented yet!
NICE_LEVEL=19

# TESTING_MODE changes from normal interval (with hours/days/weeks/months) to minute/hours interval
# Observe that the default cron job is activated once per 10 minutes. 
# If you want to run it in testing mode,
#  1) Have a small selection of directories you want to backup
#  2) If you want to, disable the cron job (make the script not executable for instance)
#  3) run the buppo_loop script manually
TESTING_MODE="yes"
#TESTING_MODE="no"



######
###### Directories
######

# Where to store the dumps
DUMP_DIR="/var/spool/buppo"

# Temporary directory
TMP_DIR="/tmp"

# Log directory
LOG_DIR="/var/log/buppo"

# State directory where data will be stored between runs
STATE_DIR="/var/lib/buppo"

# Lock directory
LOCK_DIR="/var/lock"




